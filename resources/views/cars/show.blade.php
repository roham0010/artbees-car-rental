@extends('layouts.app', ['title' => 'Car '.$car->model])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Car ').$car->model }}</div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="car_id">Car ID</label>
                        <input name="car_id" value="{{ $car->car_id }}" disabled type="text" class="form-control" id="car_id">
                    </div>
                    <div class="form-group">
                        <label for="model">Model</label>
                        <input name="model" type="text" value="{{ $car->model }}" disabled class="form-control" id="model">
                    </div>
                    <div class="form-group">
                        <label for="license-plate">License Plate</label>
                        <input name="license_plate" value="{{ $car->license_plate }}" disabled type="text" class="form-control" id="license-plate">
                    </div>
                    <div class="form-group">
                        <label for="gtcap">Gas Tank Capacity</label>
                        <input name="gas_tank_capacity" value="{{ $car->gas_tank_capacity }}" disabled type="text" class="form-control" id="gtcap">
                    </div>
                    <div class="form-group">
                        <label for="rented">Rent Status</label>
                        <input name="rented" value="{{ $car->rentString() }}" disabled type="text" class="form-control" id="rented">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
