@extends('layouts.app', ['title' => 'Cars List'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header">{{ __('Cars List') }} <a href="{{ url('cars/create') }}" class="btn btn-primary btn-sm float-right" role="button" aria-pressed="true">Add Car</a>
                </div>

                <div class="card-body">
                    @if(session('status'))
                        @if(session('status') == true)
                        <div class="alert alert-success">Car deleted</div>
                        @else
                        <div class="alert alert-warning">Car not deleted, Something happened</div>
                        @endif
                    @endif
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Car ID</th>
                            <th scope="col">Model</th>
                            <th scope="col">License Plate</th>
                            <th scope="col">GT-Capacity</th>
                            <th scope="col">Rent Status</th>
                            <th scope="col">Operations</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($cars as $car)
                                <tr>
                                    <td>{{ $car->id }}</td>
                                    <td>{{ $car->car_id }}</td>
                                    <td><a href="{{ url('cars/'.$car->id) }}">{{ $car->model }}</a></td>
                                    <td>{{ $car->license_plate }}</td>
                                    <td>{{ $car->gas_tank_capacity }}</td>
                                    <td>{{ $car->checkRent() }}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton{{$car->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton{{$car->id}}">
                                                <a class="dropdown-item" href="{{ url('cars/'.$car->id.'/rent') }}">Rent</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="{{ url('cars/'.$car->id.'/edit') }}">Edit</a>
                                                <form method="POST" action="{{ url('cars/'.$car->id) }}">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="dropdown-item">Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
