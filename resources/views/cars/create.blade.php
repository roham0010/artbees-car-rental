@extends('layouts.app', ['title' => 'Create Car'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Create Car') }}</div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session('status'))
                        @if(session('status') == true)
                        <div class="alert alert-success">Car is created</div>
                        @else
                        <div class="alert alert-warning">Car not created, Something happened</div>
                        @endif
                    @endif
                    <form method="POST" action="{{ url('cars') }}">
                        @csrf
                        <div class="form-group">
                            <label for="car_id">Car ID</label>
                            <input name="car_id" value="{{ old('car_id') }}" type="text" class="form-control" id="car_id">
                        </div>
                        <div class="form-group">
                            <label for="model">Model</label>
                            <input name="model" type="text" value="{{ old('model') }}" class="form-control" id="model">
                        </div>
                        <div class="form-group">
                            <label for="license-plate">License Plate</label>
                            <input name="license_plate" value="{{ old('license_plate') }}" type="text" class="form-control" id="license-plate">
                        </div>
                        <div class="form-group">
                            <label for="gtcap">Gas Tank Capacity</label>
                            <input name="gas_tank_capacity" value="{{ old('gas_tank_capacity') }}" type="text" class="form-control" id="gtcap">
                        </div>

                        <button type="submit" class="btn btn-primary">Add new Car</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
