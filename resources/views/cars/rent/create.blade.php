@extends('layouts.app', ['title' => 'Rent Car'])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Rent Car') }}</div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session('status'))
                    <div class="alert alert-success">{{ session('status') }}</div>
                    @endif
                    <form method="POST" action="{{ url('cars/'.$car->id.'/rent') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name">Customer Name</label>
                            <input name="name" value="{{ old('name', 'roham') }}" type="text" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="rent_at">Rent from</label>
                            <input name="rent_at" value="{{ old('rent_at', '2020-9-18 00:0:0') }}" type="text" class="form-control" id="rent_at">
                        </div>
                        <div class="form-group">
                            <label for="rent_to">Rent to</label>
                            <input name="rent_to" value="{{ old('rent_to', '2020-9-28 00:0:0') }}" type="text" class="form-control" id="rent_to">
                        </div>
                        <button type="submit" class="btn btn-primary">Rent The Car</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
