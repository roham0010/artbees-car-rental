<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    protected $fillable = ['car_id', 'customer_id', 'rent_at', 'rent_to'];

    protected $rentPeriodPrices = [
        8 => 10,
        24 => 8,
        48 => 6,
        72 => 5,
        96 => 4,
        120 => 2.5,
    ];

    /**
     * Relationships
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * helpers
     */
    public function calculate()
    {
        $start = Carbon::parse($this->rent_at);
        $end = Carbon::parse($this->rent_to);
        // Calculate diff hours
        $diffHours = $end->diffInHours($start);
        $rents = $this->rentPeriodPrices;

        foreach ($rents as $hour => $price) {
            // Check from which peroid of time our diff hours pass
            if ($diffHours < $hour) {
                break;
            }

            // Store latest hour if we pass from the first index of array to have and calculate the price base on this latest hour we passed
            $lastHour = $hour;
        }

        // if there is any hour we passed so we have to calculate price for two periods
        if (isset($lastHour)) {
            $totalPrice = ($lastHour * $rents[$lastHour]) + (($diffHours - $lastHour) * $price);
        } else {
            $totalPrice = $diffHours * $price;
        }

        return $totalPrice;
    }
}
