<?php

namespace App\Http\Controllers;

use App\Car;
use App\Customer;
use App\Rent;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::all();
        return view('cars.list', compact('cars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cars.create');
    }

    /**
     * Get form request data for a car
     */
    public function getCarDataFromRequest(Request $request)
    {
        return $request->only([
            'car_id',
            'model',
            'license_plate',
            'gas_tank_capacity'
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'car_id' => 'required|integer|unique:cars',
            'model' => 'required',
            'license_plate' => 'required|unique:cars|regex:/[0-9]{3}\-[A-Z]{2}\-[0-9]{2}/',
            'gas_tank_capacity' => 'required|integer',
        ]);

        // Get form data from request
        $carData = $this->getCarDataFromRequest($request);

        // Create the car from request form data
        $car = Car::create($carData);
        $status = false;
        if ($car) {
            $status = true;
        }
        return redirect()->back()->with(['status' => $status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        $car = $car->with(['latestRent.customer'])->first();

        return view('cars.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        return view('cars.edit', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $request->validate([
            'car_id' => 'required|integer|unique:cars,car_id,' . $car->id . ',id',
            'model' => 'required',
            'license_plate' => 'required|unique:cars,license_plate,' . $car->id . ',id|regex:/[0-9]{3}\-[A-Z]{2}\-[0-9]{2}/',
            'gas_tank_capacity' => 'required|integer',
        ]);
        $carData = $this->getCarDataFromRequest($request);
        $updated = $car->update($carData);

        $status = false;
        if ($updated) {
            $status = true;
        }
        return redirect()->back()->with(['status' => $status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        // Optional checks for wanted scenarios like if car is on rent shouldn't get deleted etc..
        $status = false;
        if ($car->delete()) {
            $status = true;
        }
        return redirect()->back()->with(['status' => $status]);
    }

    /**
     * Rent a car create form method
     */
    public function rentCreate(Car $car)
    {
        return view('cars.rent.create', compact('car'));
    }

    /**
     * Rent a car store method
     */
    public function rentStore(Request $request, Car $car)
    {
        $start = Carbon::parse($request->rent_at);
        $end = Carbon::parse($request->rent_to);
        // calculate time diff base on hours
        $diffHours = $end->diffInHours($start);
        if ($diffHours < 2 && $diffHours > 240) {
            $status = 'Rent period should be more than 2 hours and less than 10 days.';
        } else {
            if ($car->rented) {
                $status = 'This car is rented by someone else right now.';
            } else {
                $request->validate([
                    'name' => 'required|',
                    'rent_at' => 'required|date|before:rent_to|after:' . date('Y-m-d H:i:s'),
                    'rent_to' => 'required|date',
                ]);
                $rentData = $request->only([
                    'rent_at',
                    'rent_to'
                ]);
                $rentData['car_id'] = $car->id;

                $status = DB::transaction(function () use ($request, $rentData, $car) {
                    $status = "Something went wrong.";

                    // Create a new customer of it it exists grab it
                    $customer = Customer::firstOrCreate(['name' => $request->name]);
                    $rentData['customer_id'] = $customer->id;

                    // Create rent row and update cars rented status
                    $rent = Rent::create($rentData);
                    $carUpdate = $car->update(['rented' => true]);

                    // Check if all create and updates are affected
                    if ($customer && $rent && $carUpdate) {
                        $rentPrice = $rent->calculate();
                        $status = "Car successfully rented. Total price of this rent is: $" . $rentPrice;
                    }
                    return $status;
                });
            }
        }
        return redirect()->back()->with(['status' => $status]);
    }
}
