<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        'car_id', 'model', 'license_plate', 'gas_tank_capacity', 'rented'
    ];

    /**
     * relationships
     */
    public function latestRent()
    {
        return $this->hasOne('App\Rent')->latest();
    }

    /**
     * helpers
     */
    public function checkRent()
    {
        return $this->rented ? 'rented' : 'free';
    }

    public function rentString()
    {
        $lRent = $this->latestRent;
        return $this->rented ? 'Rented by ' . $lRent->customer->name . ' from "' . $lRent->rent_at . '" to "' . $lRent->rent_to . '"' : 'free';
    }
}
